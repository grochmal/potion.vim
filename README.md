README for potion-vim

## Potion Vim plugin

`potion-vim` is a plugin base on the book Learn Vim Script the Hard Way by
Steve Loch.  The book is available online at:
<http://learnvimscriptthehardway.stevelosh.com>, also, the same page describes
how to acquire a paper copy of the book.

It is an awesome book, completely worth it.

The plugin is aimed at facilitating editing and executing potion code in Vim.
Potion is a small programming language created by \_why the lucky stiff.
Potion is available at <https://github.com/perl11/potion>.

## Copying

Copyright (C) 2016 Michal Grochmal

This file is part of `potion-vim`.

`potion-vim` is free software; you can redistribute and/or modify all or parts
of it under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the License, or (at your option)
any later version.

`potion-vim` is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

The COPYING file contains a copy of the GNU General Public License.  If you
cannot find this file, see <http://www.gnu.org/licenses/>.

