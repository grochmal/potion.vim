if exists("b:current_syntax")
  finish
endif

syntax iskeyword @,48-57,_,192-255

syntax keyword potionKeyword loop times to while
syntax keyword potionKeyword if elsif else
syntax keyword potionKeyword class return

syntax keyword potionFunction print join string

syntax match potionComment "\v#.*$" contains=@Spell

syntax match potionOperator "\v\+"
syntax match potionOperator "\v\-"
syntax match potionOperator "\v\*"
syntax match potionOperator "\v/"
syntax match potionOperator "\v\?"
syntax match potionOperator "\v\="
syntax match potionOperator "\v:"
syntax match potionOperator "\v\."
syntax match potionOperator "\v\+\="
syntax match potionOperator "\v\-\="
syntax match potionOperator "\v\*\="
syntax match potionOperator "\v/\="

syntax match potionNumber "\v[-+]?\d+"
syntax match potionNumber "\v0x[\da-fA-F]+"
syntax match potionNumber "\v[-+]?\d+\.\d+"
syntax match potionNumber "\v[-+]?\d+[eE][-+]?\d+"
syntax match potionNumber "\v[-+]?\d+\.\d+[eE][-+]?\d+"

syntax region potionString start=/\v"/ skip=/\v\\./ end=/\v"/ contains=@Spell
syntax region potionString start=/\v'/ skip=/\v\\./ end=/\v'/ contains=@Spell

highlight link potionKeyword Keyword
highlight link potionFunction Function
highlight link potionComment Comment
highlight link potionOperator Operator
highlight link potionNumber Number
highlight link potionString String

let b:current_syntax = "potion"

